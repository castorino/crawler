<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\TwitterController;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $twitter = new TwitterController();
        $return = $twitter->getUserTwitter($_GET['user']);
        return new ViewModel(['twitter' => $return]);
    }
}
